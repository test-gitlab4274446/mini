package com.arielle.tata.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.arielle.tata.model.DemandeRafraichissement;
import com.arielle.tata.repository.DemandeRafraichissementRepository;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class DemandeRafraichissementServiceTest {
    @Mock
    private DemandeRafraichissementRepository demandeRepository;

    @InjectMocks
    private DemandeRafraichissementService demandeService;

    @Test
    public void testGetAllDemandes() {
        // Mock des données de test
        DemandeRafraichissement demande = new DemandeRafraichissement();
        demande.setId(1L);
        demande.setEnvironment("TestEnv");
        demande.setReason("TestReason");

        when(demandeRepository.findAll()).thenReturn(Collections.singletonList(demande));

        // Appel de la méthode à tester
        List<DemandeRafraichissement> demandes = demandeService.getAllDemandes();

        // Vérification des résultats
        assertThat(demandes).isNotNull();
        assertThat(demandes).hasSize(1);

        DemandeRafraichissement resultDemande = demandes.get(0);
        assertThat(resultDemande.getId()).isEqualTo(1L);
        assertThat(resultDemande.getEnvironment()).isEqualTo("TestEnv");
        assertThat(resultDemande.getReason()).isEqualTo("TestReason");
    }
}
