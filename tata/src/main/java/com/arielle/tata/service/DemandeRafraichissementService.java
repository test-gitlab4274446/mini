package com.arielle.tata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arielle.tata.model.DemandeRafraichissement;
import com.arielle.tata.repository.DemandeRafraichissementRepository;
import java.util.List;


@Service
public class DemandeRafraichissementService {
    private final DemandeRafraichissementRepository demandeRepository;

    @Autowired
    public DemandeRafraichissementService(DemandeRafraichissementRepository demandeRepository) {
        this.demandeRepository = demandeRepository;
    }

    public void saveDemande(DemandeRafraichissement demande) {
        // Ajoutez ici la logique métier avant d'enregistrer la demande
        // Par exemple, validation de la demande, génération d'un numéro de suivi, etc.
        
        demandeRepository.save(demande);
    }

    public List<DemandeRafraichissement> getAllDemandes() {
        return demandeRepository.findAll();
    }
}
