package com.arielle.tata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;

import com.arielle.tata.model.DemandeRafraichissement;
import com.arielle.tata.service.DemandeRafraichissementService;



@Controller
public class DemandeController {
    @Autowired
    private DemandeRafraichissementService demandeService;

    @GetMapping("/demandes")
    public String showDemandes(Model model) {
        List<DemandeRafraichissement> demandes = demandeService.getAllDemandes();
        model.addAttribute("demandes", demandes);
        return "liste-demandes";
    }
}
