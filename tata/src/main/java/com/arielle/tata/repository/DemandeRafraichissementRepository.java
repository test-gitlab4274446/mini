package com.arielle.tata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.arielle.tata.model.DemandeRafraichissement;

@Repository

public interface DemandeRafraichissementRepository extends JpaRepository<DemandeRafraichissement, Long> {
    // Ajoutez des méthodes de requête personnalisées si nécessaire
}